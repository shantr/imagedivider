﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.IO;

namespace ImageDivide2
{
    public class BitmapParser
    {
        //Канал, строка, столбец
        private static byte[] GetBitmapArray(Bitmap bmp, System.Drawing.Imaging.PixelFormat pxf = System.Drawing.Imaging.PixelFormat.Format24bppRgb)
        {
            //Пояснения см в методе MakeGray
            Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
            BitmapData bmpData = bmp.LockBits(rect, ImageLockMode.ReadWrite, pxf);
            IntPtr ptr = bmpData.Scan0;

            int numBytes = bmpData.Stride * bmp.Height;
            int widthBytes = bmpData.Stride;
            byte[] rgbValues = new byte[numBytes];

            Marshal.Copy(ptr, rgbValues, 0, numBytes);
            bmp.UnlockBits(bmpData);
            return rgbValues;
        }

        private static byte[,,] Get3DArray(byte[] array, int rows, int cols, int channels = 3)
        {
            byte[,,] result = new byte[channels, rows, cols];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    for (int channel = 0; channel < channels; channel++)
                    {
                        result[channel, i, j] = array[channels * (i * cols + j) + channel];
                    }
                }
            }
            return result;
        }

        private static Bitmap GetSlowImage(byte[,,] array)
        {
            int channels = array.GetLength(0);
            int rows = array.GetLength(1);
            int cols = array.GetLength(2);
            Bitmap result = new Bitmap(cols, rows);
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    //В итоговом массиве, полученным методом Get3DArray каналы нумеруются в обратном порядке
                    Color color = Color.FromArgb(array[2, i, j], array[1, i, j], array[0, i, j]);
                    result.SetPixel(j, i, color);
                }
            }
            return result;
        }

        public static byte[,,] GetColourImage(byte[,] array, int channels = 3)
        {
            int rows = array.GetLength(0);
            int cols = array.GetLength(1);
            byte[,,] result = new byte[channels, rows, cols];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    for (int channel = 0; channel < channels; channel++)
                    {
                        result[channel, i, j] = array[i, j];    //Во все каналы устанавливается одинаковое значение
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Преобразование из матрицы в картинку
        /// </summary>
        /// <param name="Image">Матрица</param>
        /// <returns>Изображение</returns>
        public static Bitmap GetBitmapFromTensor(byte[,] Image, int channels = 3)
        {
            return GetBitmapFromTensor(GetColourImage(Image, channels));
        }

        /// <summary>
        /// Преобразование из булевой матрицы в картинку
        /// </summary>
        /// <param name="Image">Матрица</param>
        /// <returns>Изображение</returns>
        public static Bitmap GetBitmapFromBool(bool[,] Image, System.Drawing.Color trueColor, System.Drawing.Color falseColor)
        {
            int rows = Image.GetLength(0);
            int cols = Image.GetLength(1);
            Bitmap bmp = new Bitmap(cols, rows);
            BitmapData bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            int stride = bmpData.Stride;
            System.IntPtr Scan0 = bmpData.Scan0;
            try
            {
                unsafe
                {
                    byte* p = (byte*)(void*)Scan0;
                    int nOffset = stride - bmp.Width * 3;
                    for (int y = 0; y < bmp.Height; ++y)
                    {
                        for (int x = 0; x < bmp.Width; ++x)
                        {
                            if (Image[y, x])
                            {
                                p[2] = trueColor.R;
                                p[1] = trueColor.G;
                                p[0] = trueColor.B;
                            }
                            else
                            {
                                p[2] = falseColor.R;
                                p[1] = falseColor.G;
                                p[0] = falseColor.B;
                            }

                            p += 3;
                        }
                        p += nOffset;
                    }
                }
            }
            catch { }
            bmp.UnlockBits(bmpData);
            return bmp;
        }

        //public static byte[] ConvertBitmapSourceToByteArray(BitmapSource image)
        //{
        //    byte[] data;
        //    BitmapEncoder encoder = new BmpBitmapEncoder();
        //    encoder.Frames.Add(BitmapFrame.Create(image));
        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        encoder.Save(ms);
        //        data = ms.ToArray();
        //    }
        //    return data;
        //}

        //private static BitmapImage ConvertByteArrayToBitmapImage(byte[] imageData)
        //{
        //    BitmapImage image = null;
        //    using (System.IO.MemoryStream stream = new System.IO.MemoryStream(imageData))
        //    {
        //        image = new BitmapImage();
        //        image.BeginInit();
        //        image.StreamSource = stream;
        //        image.EndInit();
        //    }
        //    return image;

        //    //var stream = new MemoryStream(imageData);
        //    //stream.Seek(0, SeekOrigin.Begin);
        //    //var image = new BitmapImage();
        //    //image.BeginInit();
        //    //image.StreamSource = stream;
        //    //image.EndInit();
        //    //stream.Close();
        //    //return image;

        //    //if (imageData == null || imageData.Length == 0) return null;
        //    //var image = new BitmapImage();
        //    //using (var mem = new MemoryStream(imageData))
        //    //{
        //    //    mem.Position = 0;
        //    //    image.BeginInit();
        //    //    image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
        //    //    image.CacheOption = BitmapCacheOption.OnLoad;
        //    //    image.UriSource = null;
        //    //    image.StreamSource = mem;
        //    //    image.EndInit();
        //    //}
        //    //image.Freeze();
        //    //return image;
        //}

        /// <summary>
        /// Преобразование из матрицы в картинку
        /// </summary>
        /// <param name="Image">Матрица</param>
        /// <returns>Изображение</returns>
        public static Bitmap GetBitmapFromTensor(byte[,] Image)
        {
            int rows = Image.GetLength(0);
            int cols = Image.GetLength(1);
            Bitmap bmp = new Bitmap(cols, rows);
            BitmapData bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            int stride = bmpData.Stride;
            System.IntPtr Scan0 = bmpData.Scan0;
            try
            {
                unsafe
                {
                    byte* p = (byte*)(void*)Scan0;
                    int nOffset = stride - bmp.Width * 3;
                    for (int y = 0; y < bmp.Height; ++y)
                    {
                        for (int x = 0; x < bmp.Width; ++x)
                        {
                            try
                            {
                                p[2] = Image[y, x];
                                p[1] = Image[y, x];
                                p[0] = Image[y, x];
                            }
                            catch { }

                            p += 3;
                        }
                        p += nOffset;
                    }
                }
            }
            catch { }
            bmp.UnlockBits(bmpData);
            return bmp;
        }

        /// <summary>
        /// Преобразование из матрицы в картинку
        /// </summary>
        /// <param name="Image">Матрица</param>
        /// <param name="colors">Цвета</param>
        /// <returns>Изображение</returns>
        public static Bitmap GetBitmapFromTensor(byte[,] Image, Color[] colors)
        {
            int rows = Image.GetLength(0);
            int cols = Image.GetLength(1);
            Bitmap bmp = new Bitmap(cols, rows);
            BitmapData bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            int stride = bmpData.Stride;
            System.IntPtr Scan0 = bmpData.Scan0;
            try
            {
                unsafe
                {
                    byte* p = (byte*)(void*)Scan0;
                    int nOffset = stride - bmp.Width * 3;
                    for (int y = 0; y < bmp.Height; ++y)
                    {
                        for (int x = 0; x < bmp.Width; ++x)
                        {
                            Color color = colors[Image[y, x]];
                            try
                            {
                                p[2] = color.R;
                                p[1] = color.G;
                                p[0] = color.B;
                            }
                            catch { }

                            p += 3;
                        }
                        p += nOffset;
                    }
                }
            }
            catch { }
            bmp.UnlockBits(bmpData);
            return bmp;
        }

        /// <summary>
        /// Преобразование из матрицы в картинку
        /// </summary>
        /// <param name="Image">Матрица</param>
        /// <param name="colors">Цвета</param>
        /// <returns>Изображение</returns>
        public static Bitmap GetBitmapFromTensor(int[,] Image, Color[] colors)
        {
            int rows = Image.GetLength(0);
            int cols = Image.GetLength(1);
            Bitmap bmp = new Bitmap(cols, rows);
            BitmapData bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            int stride = bmpData.Stride;
            System.IntPtr Scan0 = bmpData.Scan0;
            try
            {
                unsafe
                {
                    byte* p = (byte*)(void*)Scan0;
                    int nOffset = stride - bmp.Width * 3;
                    for (int y = 0; y < bmp.Height; ++y)
                    {
                        for (int x = 0; x < bmp.Width; ++x)
                        {
                            Color color = colors[Image[y, x]];
                            try
                            {
                                p[2] = color.R;
                                p[1] = color.G;
                                p[0] = color.B;
                            }
                            catch { }

                            p += 3;
                        }
                        p += nOffset;
                    }
                }
            }
            catch { }
            bmp.UnlockBits(bmpData);
            return bmp;
        }

        /// <summary>
        /// Преобразование из тензора в картинку
        /// </summary>
        /// <param name="Image">Тензор</param>
        /// <returns>Изображение</returns>
        public static Bitmap GetBitmapFromTensor(byte[,,] Image)
        {
            int rows = Image.GetLength(1);
            int cols = Image.GetLength(2);
            Bitmap bmp = new Bitmap(cols, rows);
            BitmapData bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            int stride = bmpData.Stride;
            System.IntPtr Scan0 = bmpData.Scan0;
            try
            {
                unsafe
                {
                    byte* p = (byte*)(void*)Scan0;
                    int nOffset = stride - bmp.Width * 3;
                    for (int y = 0; y < bmp.Height; ++y)
                    {
                        for (int x = 0; x < bmp.Width; ++x)
                        {
                            try
                            {
                                p[2] = Image[0, y, x];
                                p[1] = Image[1, y, x];
                                p[0] = Image[2, y, x];
                            }
                            catch { }

                            p += 3;
                        }
                        p += nOffset;
                    }
                }
            }
            catch { }
            bmp.UnlockBits(bmpData);
            return bmp;
        }

        /// <summary>
        /// Преобразование BitmapSource в матрицу через unsafe-метод
        /// </summary>
        /// <param name="PictureLocal">Изображение</param>
        /// <returns>Массив пикселей (argb)</returns>
        //public static byte[] GetArrayFromUnsafeBitmapSource(BitmapSource PictureLocal)
        //{
        //    int bytesPerPixel = ((PictureLocal.Format.BitsPerPixel + 7) / 8);
        //    int stride = PictureLocal.PixelWidth * bytesPerPixel;
        //    int arrayLength = stride * PictureLocal.PixelHeight;
        //    byte[] tintedImage = new byte[arrayLength];
        //    byte[] originalImage = new byte[arrayLength];
        //    //код преобразования
        //    PictureLocal.CopyPixels(originalImage, stride, 0);
        //    return originalImage;
        //}

        private static byte[,,] GetTensorFromArray(byte[] arr, int rows, int cols)
        {
            int channels = 4;
            byte[,,] result = new byte[rows, cols, channels];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    for (int k = 0; k < channels; k++) {
                        int index = i * cols * channels + j * channels + k;
                        result[i, j, k] = arr[index];
                    }
                }
            }
            return result;
        }

        public static byte[,] RgbToGray(byte[,,] image)
        {
            int channels = image.GetLength(0);
            int rows = image.GetLength(1);
            int cols = image.GetLength(2);
            byte[,] result = new byte[rows, cols];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    int sum = 0;
                    for (int k = 0; k < channels; k++)
                    {
                        sum += image[k, i, j];
                    }
                    result[i, j] = (byte)(sum / channels);
                }
            }
            return result;
        }

        public static bool[,] GrayToBinary(byte[,] image, byte threshold = 128)
        {
            int rows = image.GetLength(0);
            int cols = image.GetLength(1);
            bool[,] result = new bool[rows, cols];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    result[i, j] = image[i, j] >= threshold;
                }
            }
            return result;
        }

        /// <summary>
        /// Преобразование изображения в матрицу через unsafe-метод
        /// </summary>
        /// <param name="PictureLocal">Изображение</param>
        /// <returns>Матрица</returns>
        public static byte[,,] GetMatrixFromUnsafeBitmap(Bitmap PictureLocal)
        {
            //код преобразования
            BitmapData PictureLocalData = PictureLocal.LockBits(new Rectangle(0, 0, PictureLocal.Width, PictureLocal.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            int stride = PictureLocalData.Stride;//получаем ширину заблокированных данных
            System.IntPtr Scan0 = PictureLocalData.Scan0;//получаем указатель на адрес первого пикселя картинки в памяти
            byte[,,] skl = new byte[3, PictureLocal.Height, PictureLocal.Width];// инициализация матрицы значений
            try
            {
                unsafe
                {
                    byte* p = (byte*)(void*)Scan0;
                    int nOffset = stride - PictureLocal.Width * 3;
                    for (int y = 0; y < PictureLocal.Height; ++y)
                    {
                        for (int x = 0; x < PictureLocal.Width; ++x)
                        {
                            skl[0, y, x] = (p[2]);//R
                            skl[1, y, x] = (p[1]);//G
                            skl[2, y, x] = (p[0]);//B

                            //skl[3, x, y] = (int)((p[0] + p[1] + p[2]) / 3);

                            p += 3;
                        }
                        p += nOffset;
                    }
                }
            }
            catch
            {
                //MessageBox.Show("что-то пошло не так при трансформации изображения");
            }
            PictureLocal.UnlockBits(PictureLocalData);

            return (skl);
        }

        /// <summary>
        /// Получить цветное изображение из матрицы классов пикселей
        /// </summary>
        /// <param name="array">Матрица классов</param>
        /// <param name="colors">Цвета, соответствующие каждому классу</param>
        /// <returns>Цветное изображение</returns>
        public static byte[,,] GetColourImage(byte[,] array, Color[] colors)
        {
            int rows = array.GetLength(0);
            int cols = array.GetLength(1);
            int channels = 3;
            byte[,,] result = new byte[channels, rows, cols];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    int index = array[i, j];
                    Color color = colors[index];
                    result[0, i, j] = color.R;
                    result[1, i, j] = color.G;
                    result[2, i, j] = color.B;
                }
            }
            return result;
        }

        /// <summary>
        /// Преобразовать цветное изображение в бинарное (преобразование по красному каналу)
        /// </summary>
        /// <param name="array">Цветное изображение</param>
        /// <returns>Бинарное изображение</returns>
        public static bool[,] GetBinaryImage(byte[,,] array, int channels = 3)
        {
            int rows = array.GetLength(1);
            int cols = array.GetLength(2);
            bool[,] result = new bool[rows, cols];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    byte col = array[0, i, j];
                    result[i, j] = col > 100;
                }
            }
            return result;
        }

        private static byte[] Get1DArray(bool[,] array, int channels = 3)
        {
            int rows = array.GetLength(0);
            int cols = array.GetLength(1);
            byte[] result = new byte[channels * rows * cols];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    if (array[i, j])
                    {
                        for (int channel = 0; channel < channels; channel++)
                        {
                            result[channels * (i * cols + j) + channel] = 255;    //Во все каналы устанавливается одинаковое значение
                        }
                    } else
                    {
                        for (int channel = 0; channel < channels; channel++)
                        {
                            result[channels * (i * cols + j) + channel] = 0;    //Во все каналы устанавливается одинаковое значение
                        }
                    }
                }
            }
            return result;
        }
        private static byte[] Get1DArray(byte[,] array, int channels = 3)
        {
            int rows = array.GetLength(0);
            int cols = array.GetLength(1);
            byte[] result = new byte[channels * rows * cols];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    for (int channel = 0; channel < channels; channel++)
                    {
                        result[channels * (i * cols + j) + channel] = array[i, j];    //Во все каналы устанавливается одинаковое значение
                    }
                }
            }
            return result;
        }
        private static byte[] Get1DArray(byte[,,] array)
        {
            int channels = array.GetLength(0);
            int rows = array.GetLength(1);
            int cols = array.GetLength(2);
            byte[] result = new byte[channels * rows * cols];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    for (int channel = 0; channel < channels; channel++)
                    {
                        result[channels * (i * cols + j) + channel] = array[channel, i, j];
                    }
                }
            }
            return result;
        }

        //Канал, строка, столбец
        private static Bitmap GetBitmap(byte[] array, int rows, int cols, PixelFormat pxf = PixelFormat.Format24bppRgb)
        {
            //Пояснения см в методе MakeGray
            Rectangle rect = new Rectangle(0, 0, cols, rows);
            Bitmap bmp = new Bitmap(cols, rows);
            BitmapData bmpData = bmp.LockBits(rect, ImageLockMode.ReadWrite, pxf);
            IntPtr ptr = bmpData.Scan0;

            //int numBytes = bmpData.Stride * bmp.Height;
            //int widthBytes = bmpData.Stride;

            Marshal.Copy(array, 0, ptr, array.Length);
            bmp.UnlockBits(bmpData);
            return bmp;
        }

        private static Bitmap GetBitmap(byte[,,] array, PixelFormat pxf = PixelFormat.Format24bppRgb)
        {
            byte[] arr = Get1DArray(array);
            int channels = array.GetLength(0);    //Не используется! Можно выбрать pxf по количеству каналов
            int rows = array.GetLength(1);
            int cols = array.GetLength(2);
            return GetBitmap(arr, rows, cols, pxf);
        }
        private static Bitmap GetBitmap(byte[,] array, PixelFormat pxf = PixelFormat.Format24bppRgb)
        {
            byte[] arr = Get1DArray(array);
            int rows = array.GetLength(0);
            int cols = array.GetLength(1);
            return GetBitmap(arr, rows, cols, pxf);
        }

        public static void MakeGray(Bitmap bmp)
        {
            // Задаём формат Пикселя.
            PixelFormat pxf = PixelFormat.Format24bppRgb;

            // Получаем данные картинки.
            Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
            //Блокируем набор данных изображения в памяти
            BitmapData bmpData = bmp.LockBits(rect, ImageLockMode.ReadWrite, pxf);

            // Получаем адрес первой линии.
            IntPtr ptr = bmpData.Scan0;

            // Задаём массив из Byte и помещаем в него надор данных.
            // int numBytes = bmp.Width * bmp.Height * 3; 
            //На 3 умножаем - поскольку RGB цвет кодируется 3-мя байтами
            //Либо используем вместо Width - Stride
            int numBytes = bmpData.Stride * bmp.Height;
            int widthBytes = bmpData.Stride;
            byte[] rgbValues = new byte[numBytes];

            // Копируем значения в массив.
            Marshal.Copy(ptr, rgbValues, 0, numBytes);

            // Перебираем пикселы по 3 байта на каждый и меняем значения
            for (int counter = 0; counter < rgbValues.Length; counter += 3)
            {

                int value = rgbValues[counter] + rgbValues[counter + 1] + rgbValues[counter + 2];
                byte color_b = 0;

                color_b = Convert.ToByte(value / 3);


                rgbValues[counter] = color_b;
                rgbValues[counter + 1] = color_b;
                rgbValues[counter + 2] = color_b;

            }
            // Копируем набор данных обратно в изображение
            Marshal.Copy(rgbValues, 0, ptr, numBytes);

            // Разблокируем набор данных изображения в памяти.
            bmp.UnlockBits(bmpData);
        }

        //public static void SaveImageToFile(BitmapSource image, string filePath)
        //{
        //    using (var fileStream = new FileStream(filePath, FileMode.Create))
        //    {
        //        BitmapEncoder encoder = new BmpBitmapEncoder();
        //        encoder.Frames.Add(BitmapFrame.Create(image));
        //        encoder.Save(fileStream);
        //    }
        //}

        /// <summary>
        /// Преобразовать растровую картинку из Drawing.Bitmap в Windows.Media.Imaging.BitmapSource
        /// </summary>
        /// <param name="bitmap">Растровая картинка из Drawing</param>
        /// <returns>Изображение из Imaging</returns>
        //public static BitmapSource CreateBitmapSourceFromBitmap(System.Drawing.Bitmap bitmap)
        //{
        //    if (bitmap == null)
        //        throw new ArgumentNullException("Нулевая ссылка на bitmap недопустима!");

        //    return System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
        //        bitmap.GetHbitmap(),
        //        IntPtr.Zero,
        //        System.Windows.Int32Rect.Empty,
        //        BitmapSizeOptions.FromEmptyOptions());
        //}
    }
}
