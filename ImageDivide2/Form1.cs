﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageDivide2
{
    public partial class Form1 : Form
    {
        private Point[] dividePoints = null;
        private Color[] colors;

        private Bitmap sourceBmp;
        private int testCount = 10;
        private Point animPoint=Point.Empty;
        bool isSetPointCross = false;
        bool isDivided = false;
        string testImagePath = @"C:\Users\Anton\Documents\Visual Studio 2015\Projects\ImageDivide2\ImageDivide2\eye.png";
        //Task<Color[,]> colorMatrix=null;
        string fullPath;
        public Form1()
        {
            InitializeComponent();
            //Init();
        }

        private void WriteLog(string str)
        {
            textBoxLog.AppendText(str + "\n");
        }
        //async Task<Color[,]> GetColors(Bitmap im)
        //{
        //    Color[,] res = new Color[im.Height, im.Width];
        //    for (int y = 0; y < im.Height; y++)
        //        for (int x = 0; x < im.Width; x++)
        //            res[y,x] = im.GetPixel(x, y);
        //    return res;
        //}
        Color[] GetColors(int count)
        {
            Color[] res = new Color[count];
            var white = 255 << 16;// r
            white |= 255 << 8;//g
            white |= 255 << 0;//b
            double k = 0.5;
            double s = 0.25;
            double n = count;

            for (int i = 0; i < count; i++)
            {
                int color = (int)(k * (n - i) / n + s) * white;
                byte[] rgb = BitConverter.GetBytes(color);
                res[i] = Color.FromArgb(255, rgb[1], rgb[2], rgb[3]);
            }
            return res;
        }

        void Init()
        {
            Init(testImagePath);
        }
        void Init(string filePath)
        {
            sourceBmp = new Bitmap(filePath);
            buttonDividePoints.Enabled = true;
            fullPath = filePath;
            //if (colorMatrix != null) colorMatrix.Dispose();
            //colorMatrix = GetColors(sourceBmp);
            pictureBox.Image = sourceBmp;
            isDivided = false;
        }
        private void buttonDividePoints_Click(object sender, EventArgs e)
        {
            //int pointsCount = (int)dividePointsNumericUpDown.Value;
            //WriteLog($"устанавливаю {pointsCount} точек");
            //dividePoints = ImageProcessing.GenerateRandomPoints(pointsCount, sourceBmp);// определяем положения разделяющих точек
            //                                                           //colors = GetColors(dividePoints.Length);// определяем цвета участков
            //isDivided = true;
            //for (int i = 0; i < dividePoints.Length; i++)
            //{
            //    bmp.SetPixel(dividePoints[i].X, dividePoints[i].Y, Color.Black);
            //}
            //WriteLog($"установлено {pointsCount} точек");
            int pointsCount = (int)dividePointsNumericUpDown.Value;
            dividePoints = ImageProcessing.GenerateRandomPoints(pointsCount, sourceBmp);
            isDivided = true;
            WriteLog($"установлено {pointsCount} точек");
            buttonDivide.Enabled = true;
            //if (!animPoint.IsEmpty) animationButton.Enabled = true;
        }

        private void buttonDivide_Click(object sender, EventArgs e)
        {
            if (isDivided)
            {
                WriteLog($"начинаю деление на {dividePoints.Length} участков") ;
                var images = ImageProcessing.DistributePoints(dividePoints, sourceBmp.Height, sourceBmp.Width);
                WriteLog($"изображение поделено на {dividePoints.Length} участков") ;
                int imCount = dividePoints.Length;

                WriteLog($"начинаю формирование {dividePoints.Length} изображений");
                var bitmaps = ImageProcessing.GeneratePieces(sourceBmp, imCount, images);

                WriteLog($"формирование завершено");
                if (!animPoint.IsEmpty) animationButton.Enabled = true;
                //WriteLog($"сохранение изображений");
                //for (int i = 0; i < imCount; i++)
                //{
                //    string fname1 = Path.Combine(Path.GetDirectoryName(fullPath), Path.GetFileNameWithoutExtension(fullPath) + $"_{i}");
                //    fname1 += Path.GetExtension(fullPath);
                //    bitmaps[i].Save(fname1);
                //    WriteLog($"сохранено {fname1}");
                //    //string fname2 = Path.Combine(Path.GetDirectoryName(fullPath), Path.GetFileNameWithoutExtension(fullPath) + $"_{i}B");
                //    //fname2 += Path.GetExtension(fullPath);
                //    //binaryBMPs[i].Save(fname2);
                //}
                //WriteLog($"готово");
            }
            else WriteLog("точки деления не установлены\n");
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            labelX.Text = $"x:{e.X}";
            labelY.Text = $"y:{e.Y}";
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            Bitmap bmp = (Bitmap)pictureBox.Image;
            var col = bmp.GetPixel(e.X, e.Y);
            //var cols = sourceBmp;
            //labelColor.Text = $"color {cols[e.Y, e.X].R} {cols[e.Y, e.X].G} {cols[e.Y, e.X].B} ";
            labelGetpixel.Text = $"getpixel: {col.R} {col.G} {col.B}";
            if (e.Button==MouseButtons.Left && isSetPointCross)
            {
                if (animPoint.IsEmpty) animPoint = new Point(e.X, e.Y);
                else
                {
                    animPoint.X = e.X;
                    animPoint.Y = e.Y;
                }
                
                Cursor.Current = Cursors.Default;
                isSetPointCross = false;
                if (isDivided) animationButton.Enabled = true;
            }else if (e.Button == MouseButtons.Right && isSetPointCross)
            {
                Cursor.Current = Cursors.Default;
                isSetPointCross = false;
            }
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            //WriteLog($"начинаю деление на {dividePoints.Length} участков");
            //var images = imageProcessor.DistributePoints(dividePoints);
            //WriteLog($"изображение поделено на {dividePoints.Length} участков");
            //int imCount = dividePoints.Length;
            //var colors = await colorMatrix;
            //WriteLog($"начинаю формирование {dividePoints.Length} изображений");
            //var bitmaps = imageProcessor.GetBinaryImages(imCount, images);
            //WriteLog($"сохранение изображений");
            //for (int i = 0; i < imCount; i++)
            //{
            //    string fname = Path.Combine(Path.GetDirectoryName(fullPath), Path.GetFileNameWithoutExtension(fullPath) + $"_{i}B");
            //    fname += Path.GetExtension(fullPath);
            //    bitmaps[i].Save(fname);
            //    WriteLog($"сохранено {fname}");
            //}
            //WriteLog($"готово");
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            //e.Graphics.DrawImage()
        }

        private void openFileButton_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Init(openFileDialog1.FileName);
                buttonDivide.Enabled = false;
            }
        }

        private void animationButton_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.Cross;
            isSetPointCross = true;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            animPoint = Point.Empty;
            isSetPointCross = false;
            animationButton.Enabled = false;
        }
    }
}
