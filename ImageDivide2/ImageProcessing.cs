﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ImageDivide2
{
    public static class ImageProcessing
    {
        /// <summary>
        /// Генерация случайных точек в непрозрачных областях изображения
        /// </summary>
        /// <param name="pointCount">Количество точек</param>
        /// <param name="image">Изображение</param>
        /// <returns></returns>
        public static Point[] GenerateRandomPoints(int pointCount, Bitmap image)
        {
            Random rnd = new Random();

            Point[] points = new Point[pointCount];
            int width = image.Width;
            int height = image.Height;
            for(int i=0;i<pointCount; i++)
            {
                points[i] = new Point();
                int x, y;
                do
                {
                    x = (int)(rnd.NextDouble() * width);
                    y = (int)(rnd.NextDouble() * height);

                } while (image.GetPixel(x, y).A != 255);
                points[i].X = x;
                points[i].Y = y;
            }
            return points;
        }

        /// <summary>
        /// Разбиваем изображение на куски Вороного
        /// </summary>
        /// <param name="points">Заданные точки</param>
        /// <param name="rows">Количество строк</param>
        /// <param name="cols">Количество столбцов</param>
        /// <returns></returns>
        public static short[][] DistributePoints(Point[]points, int rows, int cols)
        {
            short[][] res = new short[rows][];
            for (int i = 0; i < rows; i++) res[i] = new short[cols];
            for(int i = 0; i < rows; i++)
                for(int j = 0; j < cols; j++)
                {
                    res[i][j] =(short) GetNearPoint(j, i, points);
                }
            return res;
        }
        private static int Sqr(int x) { return x * x; }
        private static int GetNearPoint(int x, int y, Point[] points)
        {
            int min2 = Sqr(points[0].X - x) + Sqr(points[0].Y - y);
            int result = 0;
            for(int i=1; i < points.Length; i++)
            {
                int r2= Sqr(points[i].X - x) + Sqr(points[i].Y - y);
                if (r2 < min2)
                {
                    min2 = r2;
                    result = i;
                }
            }
            return result;
        }

        /// <summary>
        /// Гененрируем куски
        /// </summary>
        /// <param name="source">Исходное изображение</param>
        /// <param name="imCount">Количество кусков</param>
        /// <param name="image">Сгенерированная матрица принадлежности кусков</param>
        /// <returns></returns>
        public static Bitmap[] GeneratePieces(Bitmap source, int imCount, short[][] image)
        {
            Bitmap[] images = new Bitmap[imCount];
            int height = image.Length;
            int width = image[0].Length;
            for (int i = 0; i < imCount; i++)
            {
                images[i] = new Bitmap(width, height);
                images[i].MakeTransparent(Color.Black);
                //for (int j = 0; j < width; j++) images[i][j] = new bool[width];
            }

            for (int y = 0; y < height; y++)
                for (int x = 0; x < width; x++)
                {
                    short index = image[y][x];
                    images[index].SetPixel(x, y, source.GetPixel(x, y));

                }
            return images;
        }
    }
}
